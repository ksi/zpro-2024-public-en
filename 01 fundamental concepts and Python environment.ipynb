{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fundamental concepts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computer\n",
    "\n",
    "[Computer](https://en.wikipedia.org/wiki/Computer) is an electronic digital machine that can be programmed to perform sequence of arithmetic and logical operations (computations) automatically.\n",
    "Modern computers are capable of performing generic sets of instructions known as programs.\n",
    "Such programs allow computers to solve broad variety of problems.\n",
    "\n",
    "The physical equipment of a computer is known as a  __hardware__ and can have wide variety of forms ranging from servers, through personal computers and laptops to portable devices such as cell phones or smart watches.\n",
    "\n",
    "__Software__ represents program equipment of computers. It consists of operating system, user programs, and data.\n",
    "\n",
    "The majority of today's computers is built on the concept of [von Neumann architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture):\n",
    "\n",
    "![](images/01/Von_Neumann_Architecture.svg)\n",
    "\n",
    "- central processing unit (CPU) consists of control unit and arithmetic-logical unit (ALU), \n",
    "- memory stores program as well as data (in Von Neumann Architecture!)\n",
    "- external input and output (I/O) devices (peripherals) allow interaction of computer with environment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Program\n",
    "\n",
    "[Computer program](https://en.wikipedia.org/wiki/Computer_program) is a sequence of instructions that describe the solution of a certain problem by computer.\n",
    "In order to perform some activity, the computer needs to have at least one program in its memory.\n",
    "Nowadays, computers contain a kernel of an operating system in memory.\n",
    "The kernel controls the computer and allows execution of user programs (application software)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algorithm\n",
    "\n",
    "[Algorithm](https://en.wikipedia.org/wiki/Algorithm) is a set of exact instructions that are used to solve a class of specific problems.\n",
    "The term is frequently used in computer science - it represents theoretical solution of some specific problem.\n",
    "The algorithm does not depend on any programming language.\n",
    "In general, the algorithm can appear in other domains, both scientific and non-scientific - e.g. recipe in cookbook.\n",
    "\n",
    "Typically, we demand that the following properties are fulfilled:\n",
    "\n",
    "1. __Elementary:__\n",
    "   Algorithms consist of simple - elementary - steps. The processor (i.e. device that will execute the algorithm) can execute these elementary steps.\n",
    "   \n",
    "2. __Finite:__\n",
    "   Each algorithm must end in a finite number of steps. This number may be arbitrary large, but must be finite for any possible input of the algorithm.\n",
    "\n",
    "3. __Generality (universality):__\n",
    "   An algorithm does not solve a single problem (such as calculation of product 3×7), instead it solves an entire class of problems (e.g. calculation of the product of two arbitrary integers).\n",
    "\n",
    "4. __Definiteness:__\n",
    "   Each step of the algorithm must be unambiguous. In each situation, it must be clear by which step the algorithm will continue.\n",
    "\n",
    "5. __Output:__\n",
    "   Each algorithm has at least one output which is related to its inputs. The execution of an algorithm transforms inputs to the output.\n",
    "\n",
    "Algorithms can be informally described using [pseudocode](https://en.wikipedia.org/wiki/Pseudocode) or [flowcharts](https://en.wikipedia.org/wiki/Flowchart).\n",
    "For example, the computation of factorial:\n",
    "\n",
    "1. Read value of integer $N$.\n",
    "2. Set numbers $M=1$ a $F=1$.\n",
    "3. Compute product $M \\times F$ and store the result in the $F$.\n",
    "4. Check, if numbers $M$ and $N$ are equal:\n",
    "   - if yes, continue to step 5.\n",
    "   - if no, increase number $M$ by 1 and continue to step 3.\n",
    "5. Say that the result is stored in $M$ and terminate the algorithm.\n",
    "\n",
    "<div>\n",
    "<img src=\"images/01/Factorial Flowchart.png\" style=\"max-width: 30em\"/>\n",
    "</div>\n",
    "\n",
    "### Algorithm analysis\n",
    "\n",
    "In the summer semester, you may enroll in the 18ZALG course that focuses on the analysis of algorithms.\n",
    "Advanced analysis and complexity theory are also presented in 01TSLO, 01PAA, 01PALG, 18UIA1, and 18UIA2 courses in Master's degree study programme (only some specializations)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Programming languages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Programming language](https://en.wikipedia.org/wiki/Programming_language) is a system of notations that allows writing algorithms in order to execute them on a computer.\n",
    "Programming language is a communication tool that interconnects the programmer who uses it to describe a solution of given problem and computer who uses its technical capabilities to execute the described solution (computer program).\n",
    "\n",
    "In the era of first computers, it was necessary to write programs directly in the _machine code_, later the _language of symbolic addresses/instructions_ (_assembler_) has been developed and even later, first _high level languages_ have been created.\n",
    "This trend of increasing the level of abstraction still continues, therefore the effectivity of programmers' work is more important than the effectivity of processing code by computers.\n",
    "__Consequently, it is essential to write code in a way that other programmers can read it and understand it.__\n",
    "\n",
    "Programming languages can be divided according to different criteria:\n",
    "\n",
    "- according to the level of abstraction: low level and high level\n",
    "- according to the way of code execution: compiled and interpreted\n",
    "- according to the domain of usage: multipurpose and domain specific\n",
    "\n",
    "High level languages can be further divided into many subgroups according to particular language properties or code notation details.\n",
    "\n",
    "Most popular languages according to the [TIOBE index](https://www.tiobe.com/tiobe-index/):\n",
    "\n",
    "![](images/01/Tiobe_2024.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python language\n",
    "\n",
    "[Python](https://www.python.org/) is high-level, interpreted, multipurpose programming language that focuses on the purity and readability of the source code.\n",
    "\n",
    "Python's popularity has increased since 2008 and now it regularly appears between the most popular languages.\n",
    "\n",
    "Python is an [open source](https://en.wikipedia.org/wiki/Open-source_software) project a is freely available for all major platforms (Linux, macOS, Windows).\n",
    "Lot of open source libraries, frameworks, and other software projects using Python exists.\n",
    "This contributes to the popularity of the Python and its software ecosystem.\n",
    "\n",
    "Python is an __interpreted__ language, an __interpreter__ is required to run the code in Python.\n",
    "Interpreter is a program that reads the source code and executes (interprets) it step by step.\n",
    "The interpretation comes with several consequences, namely:\n",
    "\n",
    "- since an interpreter is required to run the code, code execution is slower compared to compiled programs\n",
    "- it is easy to modify the program (modified code can be run instantly)\n",
    "- source codes are easily portable to different platforms (compatibility is achieved by the interpreter)\n",
    "- to run the program, its complete source code is required (it is not possible to create a closed \"binary\" version of the program)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## C a C++ languages\n",
    "\n",
    "The [C](https://en.wikipedia.org/wiki/C_(programming_language)) and [C++](https://en.wikipedia.org/wiki/C%2B%2B) languages belong to the most widespread programming languages.\n",
    "\n",
    "Same as Python, C and C++ are high-level, general purpose languages, but in contrast to Python, they are __compiled__.\n",
    "The __compiler__ is required to run the programs written in the C/C++ languages.\n",
    "The compiler processes the source code and generates a binary executable file (in Windows typically with the `.exe` extension) containing machine code for given architecture.\n",
    "\n",
    "C and C++ are considered to be high-level languages, however in comparison to the Python language, they look much lower level.\n",
    "C and C++ languages are typically used in domains that have high demands on the performance of code execution - e.g. operating systems, hardware drivers, or various native applications.\n",
    "\n",
    "The most widespread Python interpreter is created in the C language and is called  _CPython_.\n",
    "\n",
    "In the second year of bachelors degree programme, there are courses 18PRC1 and 18PRC2 dedicated to programming in the C++ language."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Programming languages taught on FNSPE\n",
    "\n",
    "You can learn other programming languages in the following courses offered by the Department of Software Engineering at the Faculty of Nuclear Sciences and Physical Engineering:\n",
    "\n",
    "- Python – 18ZPRO, 18PPY1, 18PPY2, 18PPY3\n",
    "- C++ – 18PRC1, 18PRC2\n",
    "- Java – 18PJ\n",
    "- JavaScript – 18INTA\n",
    "- Matlab – 18PMTL\n",
    "- SQL – 18SQL\n",
    "\n",
    "Detailed synopsis of the courses can be found in the [White Book](https://bilakniha.cvut.cz/cs/katedra14118.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python Environment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The environment of the [Python](https://www.python.org/) language consists of several components:\n",
    "\n",
    "- the programming language itself\n",
    "- interpreter of the language and set of other tools (interactive shell, simple integrated development environment (IDE), etc.)\n",
    "- standard library (set of modules containing many useful functions, classes, and other objects)\n",
    "- system for maintenance and distribution of packages (user libraries and packagesy)\n",
    "- [documentation](https://docs.python.org/3/)\n",
    "\n",
    "The Python language ay be used in several ways:\n",
    "\n",
    "1. interactive work in the command prompt - _shell_ (the REPL principle – _read-evaluate-print loop_)\n",
    "2. scripting – creation of simple programs that usually automates work in the operating system (e.g. copying or renaming files)\n",
    "3. creation of programs (application)\n",
    "4. hybrid work in systems with built-in Python interpreter (e.g. _Jupyter_ environment)\n",
    "\n",
    "All these approaches will be demonstrated during this course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Jupyter environment\n",
    "\n",
    "During the course, we will use the JupyterHub platform, which can be accessed at https://jupyter.fjfi.cvut.cz/.\n",
    "To login, use your University username and [CTU password](https://it.fjfi.cvut.cz/en/navody/uzivatelsky-ucet/ucet-cvut).\n",
    "Problems with using the platform can be reported to your lecturer or directly to the administrator of the platform (Jakub Klinkovský).\n",
    "\n",
    "The [Jupyter](http://jupyter.org/) environment consists of several components:\n",
    "\n",
    "1. JupyterHub – web service (_front-end_) providing access to the other components.\n",
    "2. JupyterLab – new interface providing functionality available in traditional desktop integrated development environments (IDE).\n",
    "3. Jupyter Notebook – classical environment that allows work on single document.\n",
    "\n",
    "After logging in to the JupyterHub service, the Jupyter Server process process is started with the JupyterLab interface.\n",
    "This interface can be identified by the `/lab/` string in the URL in the address bar of the browser, e.g. https://jupyter.fjfi.cvut.cz/user/novak123/lab/.\n",
    "To switch to the Jupyter Notebook, select `View → Open in Jupyter Notebook` or `Help → Launch Jupyter Notebook File Browser` in the menu.\n",
    "This will open a new URL containing the `/tree/` substring, e.g. https://jupyter.fjfi.cvut.cz/user/novak123/tree/.\n",
    "To switch back to the JupyterLab interface, select `View → Open in JupyterLab` in the menu.\n",
    "\n",
    "When the Jupyter Server is launched, either the previous _workspace_ is restored, or a _launcher_ tab with icons is started:\n",
    "<div>\n",
    "<img src=\"images/01/JupyterLab-launcher.png\" style=\"max-width: 42em\"/>\n",
    "</div>\n",
    "\n",
    "The first row of icons represents launchers for basic elements of the Jupyter environment, so called _notebooks_.\n",
    "Jupyter notebook is a document that consists of an ordered list of _cells_ that have content of various types.\n",
    "\n",
    "Cells can contain text (formatted by the [Markdown](https://en.wikipedia.org/wiki/Markdown) or [HTML](https://en.wikipedia.org/wiki/HTML)) or can contain source code in some programming language, in our case Python.\n",
    "To execute the code in the cells of the notebook, the so called _kernel_ is required. It is a process capable of executing statements of given programming language (in our case, it is a Python interpreter).\n",
    "After executing some cell containing a source code, the output is displayed - it may be some text, the value of some _variable_, or any multimedia data that Jupyter can display.\n",
    "\n",
    "Jupyter notebooks are represented by files with the `.ipynb` extension. Notebooks from the JupyterHub server can be downloaded and opened locally using the appropriate editor such as local installation of JupyterLab or Jupyter Notebook components. Also, the VS Code IDE can be used. We will use the VS Code later in this course.\n",
    "\n",
    "The second row of icons on the launcher provides different means of work with particular programming languages. Running interactive console provides interactive mode for work with the interpreter that is based on the REPL principle (_read-evaluate-print loop_).\n",
    "\n",
    "The same interface can be run locally (not on web) directly after the installation of the Python on your system.\n",
    "\n",
    "\n",
    "### First examples\n",
    "\n",
    "We will demonstrate very simple examples how to use the Python language. We will start with the interactive shell to:\n",
    "\n",
    "- use the `print` function to show some information on the output\n",
    "- write some elementary mathematical expressions\n",
    "- use variables\n",
    "\n",
    "Then, we repeat the same examples in the notebook _notebook_ document opened in the Jupyter notebook interface. We will also show how to:\n",
    "\n",
    "- add, copy, move cells\n",
    "- change type of the cell\n",
    "- execute the code stored in the cell\n",
    "- show help (documentation) for given function"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
