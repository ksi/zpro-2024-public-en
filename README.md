# ZPRO 2024

This page will contain all information related to the **English** version of 18ZPRO – announcements, course materials, etc.

The contents of this project can be easily synchronized into the [JupyterHub](https://jupyter.fjfi.cvut.cz/) environment by clicking on the following button:

[![Static Badge](https://img.shields.io/badge/Synchronize%20to%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jlk.fjfi.cvut.cz/jupyter/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fzpro-2024-public-en.git&urlpath=lab%2Ftree%2Fzpro-2024-public-en.git%2FREADME.md&branch=main)

## Information regarding the course

We will meet every Monday at 14:00 and every Thursday at 10:00 in the classroom T-105.
Attendance is not compulsory, but highly recommended.
The evaluation of your progress through the course will be based on home assignments (12 per term) and three quick (15 minutes) tests.
If you manage to get at least 90% of the points from both home assignments and tests, you will receive the "zápočet" and credits.
If you receive at least 50% of the points from both home assignments and test, you will have to additionally show your knowledge in a short oral defense.
In case you will receive less than 50% of points from home assignments or tests, you will have to submit and defend an additonal task that will be assigned individually.

In the context of the Introduction to programming course, using tools based on AI (Chatbot, copilot) is considered same as the cooperation with a human.
You should solve the home assignments on your own.
You can use AI for help, but not for solving the assignments.
During the tests, use of AI and cooperation with someone else will not be permitted.

## Announcements

- The first test will be written on Monday, October 21 and Tuesday, October 22.
  The standard time for completing the test will be 15 minutes and it will contain 12 simple questions from the topics we have covered so far.
  You can get 0-10 points for each question, but there is a maximum of 100 points for the whole test (two questions are bonus, where it is possible to get lost points).
  You can find a sample version of the questions in the file [tests/demo1.pdf](tests/demo1.pdf).
  The test will be in paper form and it will not be possible to use any electronic aids.
- The second test will be written on Monday, November 25 and Tuesday, November 26.
  The form of the test will be the same as before.
  You can find a sample version of the questions in the file [tests/demo2.pdf](tests/demo2.pdf).
- The third and last test will be written on Wednesday, December 11, Thursday, December 12, and
  Friday, December 13. The test will have the same form as the previous two tests.
  You can find a sample version of the questions in the file [tests/demo3.pdf](tests/demo3.pdf).

## Links

- ["White Book"](https://bilakniha.cvut.cz/en/predmet11274505.html) – study plans, supplementary literature
- [GitLab project](https://gitlab.fjfi.cvut.cz/ksi/zpro-2024-public-en)
- [Introductory lecture (in Czech)](00_PT.pdf)
- [Information about CTU user account](https://it.fjfi.cvut.cz/en/navody/uzivatelsky-ucet/ucet-cvut)
- [FJFI JupyterHub](https://jupyter.fjfi.cvut.cz/)
- [Anonymous Jupyter environment](https://jupyter.org/try-jupyter)

## Authors

The materials for the English variant of the course are based on the original [Czech materials](https://gitlab.fjfi.cvut.cz/ksi/zpro-2024-public).
The authors of these materials, including [previous courses](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public), are:

- Jakub Klinkovský
- Zuzana Petříčková
- Vladimír Jarý
- Maksym Dreval
- Petr Pauš
- Jan Tomsa
- František Voldřich

The materials are available under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
